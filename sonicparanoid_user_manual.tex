%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% User manual for SonicParanoid
% LaTeX Template
%
% Please find more information about SonicParanoid at:
% http://iwasakilab.bs.s.u-tokyo.ac.jp/sonicparanoid
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[12pt]{article} % Default font size is 12pt, it can be changed here


% Normal Latex
%\usepackage{helvet}
%\renewcommand{\familydefault}{\sfdefault}

%XeTeX or LuaTex
\usepackage{fontspec}
\setmainfont{Arial}

\usepackage{hyperref} % for URLs

\usepackage{mdframed} % Other package for boxes
\usepackage{todonotes} % Required for the alert boxes
\usepackage{soul} % simple text highlight

\usepackage{geometry} % Required to change the page size to A4
\geometry{a4paper} % Set the page size to be A4 as opposed to the default US Letter
%\usepackage[a4paper, margin=1in, footskip=0.25in]{geometry}
%\usepackage[a4paper, total={154mm, 210mm}]{geometry}

\usepackage{graphicx} % Required for including pictures
%\graphicspath{{Pictures/}} % Specifies the directory where pictures are stored

\usepackage{float} % Allows putting an [H] in \begin{figure} to specify the exact location of the figure
\usepackage{wrapfig} % Allows in-line images such as the example fish picture

\usepackage{lipsum} % Used for inserting dummy 'Lorem ipsum' text into the template

\linespread{1.2} % Line spacing

%\setlength\parindent{0pt} % Uncomment to remove all indentation from paragraphs

% Create alert boxes
\newcommand{\alertBoxRed}[1] % This is what you will use to create a new question
{
%\refstepcounter{questions} % Increases the questions counter, this can be referenced anywhere with \thequestions
%\par\noindent % Creates a new unindented paragraph
%\phantomsection % Needed for hyperref compatibility with the \addcontensline command
%\addcontentsline{faq}{questions}{#1} % Adds the question to the list of questions
\todo[inline, color=red!40]{\textbf{#1}} % Uses the todonotes package to create a fancy box to put the question
\vspace{1em} % White space after the question before the start of the answer
}

\newcommand{\alertBoxBlue}[1] % This is what you will use to create a new question
{
%\refstepcounter{questions} % Increases the questions counter, this can be referenced anywhere with \thequestions
%\par\noindent % Creates a new unindented paragraph
%\phantomsection % Needed for hyperref compatibility with the \addcontensline command
%\addcontentsline{faq}{questions}{#1} % Adds the question to the list of questions
\todo[inline, color=blue!20]{\textbf{#1}} % Uses the todonotes package to create a fancy box to put the question
\vspace{1em} % White space after the question before the start of the answer
}

\newcommand{\alertBoxGreen}[1] % This is what you will use to create a new question
{
%\refstepcounter{questions} % Increases the questions counter, this can be referenced anywhere with \thequestions
%\par\noindent % Creates a new unindented paragraph
%\phantomsection % Needed for hyperref compatibility with the \addcontensline command
%\addcontentsline{faq}{questions}{#1} % Adds the question to the list of questions
\todo[inline, color=green!40]{\textbf{#1}} % Uses the todonotes package to create a fancy box to put the question
\vspace{1em} % White space after the question before the start of the answer
}

\begin{document}

%----------------------------------------------------------------------------------------
%	TITLE PAGE
%----------------------------------------------------------------------------------------

\begin{titlepage}

\newcommand{\HRule}{\rule{\linewidth}{0.5mm}} % Defines a new command for the horizontal lines, change thickness here

\center % Center everything on the page

\textsc{\LARGE SonicParanoid}\\[0.5cm] % Name of your university/college
%\textsc{\Large Major Heading}\\[0.5cm] % Major heading such as course name
\textsc{\large User Guide}\\[0.5cm] % Minor heading
\textsc{\large Salvatore Cosentino \& Wataru Iwasaki}\\[0.5cm] % Minor heading
\textsc{\small http://iwasakilab.bs.s.u-tokyo.ac.jp/sonicparanoid}\\[1.0cm] % Minor heading (web-address)

%\HRule \\[0.4cm]
%{ \huge \bfseries Title}\\[0.4cm] % Title of your document
%\HRule \\[1.5cm]

%\begin{minipage}{0.4\textwidth}
%\begin{flushleft} \large
%\emph{Author:}\\
%John \textsc{Smith} % Your name
%\end{flushleft}
%\end{minipage}
%~
%\begin{minipage}{0.4\textwidth}
%\begin{flushright} \large
%\emph{Supervisor:} \\
%Dr. James \textsc{Smith} % Supervisor's Name
%\end{flushright}
%\end{minipage}\\[4cm]


{\large \today}\\[3cm] % Date, change the \today to a set date if you want to be precise

%\includegraphics{Logo}\\[1cm] % Include a department/university logo - this will require the graphicx package

\vfill % Fill the rest of the page with whitespace

\end{titlepage}

%----------------------------------------------------------------------------------------
%	TABLE OF CONTENTS
%----------------------------------------------------------------------------------------

\tableofcontents % Include a table of contents

\newpage % Begins the essay on a new page instead of on the same page as the table of contents

%----------------------------------------------------------------------------------------
%	SUMMARY
%----------------------------------------------------------------------------------------

\section{Summary} % Major section
SonicParanoid is a stand-alone software for the identification of orthologous relationships among multiple species. SonicParanoid is an open source software released under the Apache-2.0 license, implemented in Python3, Cython, and C++, and working on Linux and Mac OSX. The software is designed to run using multiple processors and proved to be up to 1245X faster then InParanoid, 166X faster than Proteinortho, and 172X faster than OrthoFinder 2.0 with an accuracy comparable to that of well-established orthology inference tools. Thanks to its speed, accuracy, and usability SonicParanoid substantially relieves the difficulties of orthology inference for biologists who need to construct and maintain their own genomic datasets.

SonicParanoid was tested on a benchmark proteome dataset provided by the Quest for Orthologs (QfO) consortium [\url{https://questfororthologs.org}], and its accuracy was assessed, and compared to that of other 13 methods, using a publicly available orthology benchmarking service (Altenhoff et al. 2016).\newline
SonicParanoid is available at: \newline \url{http://iwasakilab.bs.s.u-tokyo.ac.jp/sonicparanoid}.

%------------------------------------------------

\section{Installation} % Sub-section
In this section the installation of SonicParanoid on different operative systems
will be explained step-by-step, including the installation of the third-party software used by SonicParanoid.

%------------------------------------------------

\subsection{Hardware requirements} % Sub-section
SonicParanoid requires a system with a 64-bit multi-core CPU and 8 GB of memory.

%------------------------------------------------

\subsection{Supported operative systems} % Sub-sub-section
SonicParanoid should work on any Unix-based system provided the required software is installed and the hardware requirements are met.
SonicParanoid was tested to work on the following systems:
\begin{itemize}
  \item Apple MacOS High Sierra (10.13)
  \item Ubuntu (ver. 17.10)
  \item OpenSUSE Leap (ver. 42.3)
\end{itemize}

\subsection{Software requirements} % Sub-section
Before downloading SonicParanoid make sure that the following software is installed in your system:
\begin{itemize}
  \item Python 3.6 or above, with numpy, pandas, cython, sh, and biopython modules installed
  \item Git version control system (version 2.0 or above)
  \item GNU GCC compiler (version 5.0 or above)
\end{itemize}


%------------------------------------------------

\subsection{Installation, setup, and test on the supported operative systems} % Sub-sub-section

\alertBoxRed{Because MMseqs2 is in active development we suggest that you build the version that comes with SonicParanoid, following the instructions in this chapter.}
\alertBoxRed{On Linux systems you will require root privileges, please ask your system administrator if you do not have root privileges.}


%----------------------------------------------------------------------------------------
%	Setup Ubuntu
%----------------------------------------------------------------------------------------
\subsubsection{Ubuntu (ver. 17.10 or above)} % Sub-sub-section
\alertBoxRed{Ubuntu 17.10 comes with Python3 (ver. 3.6.3) already installed.}
\noindent \textbf{Install required software}
%\alertBoxBlue{Install git and python packages}
\begin{mdframed}[backgroundcolor=blue!20]
\$ sudo apt install git python3-pip  python3-pandas python3-biopython build-essential cmake \--\--assume-yes\newline
\$ sudo -H pip3 install -U pip setuptools sh cython
\end{mdframed}

% donwload sonicpara
\noindent \textbf{Obtain SonicParanoid}
\begin{mdframed}[backgroundcolor=blue!20]
\$ git clone https://bitbucket.org/salvocos/sonicparanoid\newline
\$ cd sonicparanoid
\end{mdframed}

% Build MMseqs2
\noindent \textbf{Compile MMseqs2 (from within the sonicparanoid directory)}
\begin{mdframed}[backgroundcolor=blue!20]
\$ cd src\newline
\$ tar -zxf mmseqs.tar.gz\newline
\$ mkdir build\newline
\$ cd build\newline
\$ cmake -DCMAKE\_BUILD\_TYPE=RELEASE \-DCMAKE\_INSTALL\_PREFIX=../.. ..\newline
\$ make\newline
\$ make install\newline
\$ cd ../../
\end{mdframed}

\noindent \textbf{Setup and test (from within the sonicparanoid directory)}
%\alertBoxGreen{Setup and test}
\begin{mdframed}[backgroundcolor=green!20]
\$ python3 setup\_sonicparanoid.py\newline
\$ python3 sonicparanoid.py -i test\_input -o test\_output -t 4 -m fast
\end{mdframed}

%----------------------------------------------------------------------------------------
%	Setup OpenSUSE
%----------------------------------------------------------------------------------------
\subsubsection{OpenSUSE Leap (ver. 42.3 or above)} % Sub-sub-section
\alertBoxRed{OpenSUSE Leap 42.3 comes with Python3 (ver. 3.4.6) already installed.}
\noindent \textbf{Install required development tools}
%\alertBoxBlue{Install required development tools}
\begin{mdframed}[backgroundcolor=blue!20]
\$ sudo zypper install -y git python3-devel\newline
\$ sudo zypper install -y gcc-c++ gcc7\newline
\$ sudo zypper install -y cmake
\end{mdframed}

% donwload sonicpara
\noindent \textbf{Obtain SonicParanoid}
\begin{mdframed}[backgroundcolor=blue!20]
\$ git clone https://bitbucket.org/salvocos/sonicparanoid\newline
\$ cd sonicparanoid
\end{mdframed}

% Build MMseqs2
\noindent \textbf{Compile MMseqs2 (from within the sonicparanoid directory)}
\begin{mdframed}[backgroundcolor=blue!20]
\$ cd src\newline
\$ tar -zxf mmseqs.tar.gz\newline
\$ mkdir build\newline
\$ cd build\newline
\$ cmake -DCMAKE\_BUILD\_TYPE=RELEASE \-DCMAKE\_INSTALL\_PREFIX=../.. ..\newline
\$ make\newline
\$ make install\newline
\$ cd ../../
\end{mdframed}

\noindent \textbf{Setup and test (from within the sonicparanoid directory)}
%\alertBoxGreen{Setup and test}
\begin{mdframed}[backgroundcolor=green!20]
\$ python3 setup\_sonicparanoid.py\newline
\$ python3 sonicparanoid.py -i test\_input -o test\_output -t 4 -m fast
\end{mdframed}

%----------------------------------------------------------------------------------------
%	Apple MacOSX
%----------------------------------------------------------------------------------------
\subsubsection{Apple MacOSX High Sierra} % Sub-sub-section
\alertBoxRed{Because there are no distributable binaries of MMseqs2 for OSX, it must be compiled locally following the steps below and using the same Git commit suggested below.}
\noindent \textbf{Install required development tools}
%\alertBoxBlue{Install required development tools}
\begin{mdframed}[backgroundcolor=blue!20]
Download and install the latest stable version of Xcode from\newline \emph{https://developer.apple.com/download}\newline
\$ Xcode install and setup
\end{mdframed}

\noindent \textbf{Install Homebrew, Python3, and Git}
\begin{mdframed}[backgroundcolor=blue!20]
\$ ruby -e "\$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"\newline
\$ export PATH=/usr/local/bin:/usr/local/sbin:\$PATH\newline
\$ brew install gcc\newline
\$ brew install cmake\newline
\$ brew install git python3
\end{mdframed}

% set highlight color
\sethlcolor{lightgray}

% donwload sonicpara
\noindent \textbf{Obtain SonicParanoid}
\begin{mdframed}[backgroundcolor=blue!20]
\$ git clone https://bitbucket.org/salvocos/sonicparanoid\newline
\$ cd sonicparanoid
\end{mdframed}

% Build MMseqs2
\noindent \textbf{Compile MMseqs2 (from within the sonicparanoid directory)}
\begin{mdframed}[backgroundcolor=blue!20]
\$ cd src\newline
\$ tar -zxf mmseqs.tar.gz\newline
\$ mkdir build\newline
\$ cd build\newline
\$ CXX="\$(brew --prefix)/bin/g++-7" cmake -DCMAKE\_BUILD\_TYPE=RELEASE -DCMAKE\_INSTALL\_PREFIX=. ..\newline
\$ make\newline
\$ make install\newline
\$ cd ../../
\end{mdframed}

\noindent \textbf{Setup and test (from within the sonicparanoid directory)}
%\alertBoxGreen{Setup and test}
\begin{mdframed}[backgroundcolor=green!20]
\$ python3 setup\_sonicparanoid.py\newline
\$ python3 sonicparanoid.py -i test\_input -o test\_output -t 4 -m fast
\end{mdframed}


%----------------------------------------------------------------------------------------
%	KNOWN ISSUES
%----------------------------------------------------------------------------------------
\sethlcolor{red}
\section{Known issues} % Major section
This section contains possible solutions to some of the known installation issues in SonicParanoid.
% ISSUE 1
\subsection*{\hl{Issue 1}}  % Sub-sub-section
\begin{mdframed}[backgroundcolor=red!20]
SonicParanoid crashed with the error 'No module named inpyranoid\_c', or 'No module named mmseqs\_parser\_c'
\end{mdframed}
\sethlcolor{green}
\subsection*{\hl{Possible solution}}  % Sub-sub-section
\alertBoxRed{Make sure that Cython has been properly installed before proceeding.}
\noindent Execute the following two commands from the directory in which SonicParanoid has been installed:
\begin{mdframed}[backgroundcolor=green!20]
\$ python3 compile\_inpyranoid\_c.py build\_ext \--\--inplace\\
\$ python3 compile\_mmseqs\_parser\_c.py build\_ext \--\--inplace
\end{mdframed}
% ISSUE 2
\sethlcolor{red}
\subsection*{\hl{Issue 2}}  % Sub-sub-section
\begin{mdframed}[backgroundcolor=red!20]
The file with the multi-species ortholog groups contains only the headers.
\end{mdframed}
\sethlcolor{green}
\subsection*{\hl{Possible solution}}  % Sub-sub-section
\alertBoxRed{Make sure that the GCC compiler has been properly installed before proceeding.}
\noindent From the directory in which SonicParanoid is installed type the following commands:
\begin{mdframed}[backgroundcolor=green!20]
\$ cd quick\_multi\_paranoid\\
\$ make clean\\
\$ make qa\\
\$ cd ..
\end{mdframed}

%----------------------------------------------------------------------------------------
%	USAGE
%----------------------------------------------------------------------------------------
\section{Usage} % Major section
\sethlcolor{lightgray}
SonicParanoid can be executed through the command line by running the script \hl{sonicparanoid.py} which is located inside the directory where the program has been installed.

\noindent The command:
\begin{mdframed}[backgroundcolor=gray!20]
python3 sonicparanoid.py \--\--help
\end{mdframed}
provides extra information on the command line parameters.

%------------------------------------------------

\subsection{Input format} % Input format
SonicParanoid input files must be valid FASTA formatted files containing protein sequences.
\begin{mdframed}[backgroundcolor=red!20]
\begin{itemize}
  \item The file names must not contain dash \hl{-} symbols nor extensions.
  \item SonicParanoid will automatically replace blank, tabulation, and additional greater-then \hl{>} symbols in the headers with pipe \hl{|} symbols.
\end{itemize}
\end{mdframed}


%------------------------------------------------

\subsection{Execution example} % Input format
The main directory contains a directory (test\_input) containing 4 bacterial proteomes that can be used to test that SonicParanoid has been successfully installed.

\noindent You can perform the test run using the following command:
\begin{mdframed}[backgroundcolor=gray!20]
python3 sonicparanoid.py -i test\_input -o test\_output -m fast -t 4
\end{mdframed}
The above command infers the orthologous relationships among the species which proteomes in FASTA format are stored in \hl{test\_input}, using 4 CPUs, and store the output in the directory \hl{test\_output}.

%------------------------------------------------

\subsection{Output} % Sub-sub-section
Given a run with N input proteomes, the main output directory will have the following structure and content:

\begin{mdframed}[backgroundcolor=blue!20]
\begin{itemize}
  \item \textit{N * (N - 1) / 2}　each containing the ortholog table for one of the possible proteome-proteome combinations for the \textit{N} input proteomes.
  \item The \hl{multi\_species} directory contains the file with the multi-species ortholog groups (\hl{multispecies\_clusters\_all.tsv})
  \item \hl{species.txt} contains all the \textit{N} input file names.
  \item \hl{species\_pairs.txt} contains a list of all the \textit{N * (N - 1) / 2} combinations for the \textit{N} input proteomes.
\end{itemize}
\end{mdframed}

%------------------------------------------------

\subsection{Command line parameters} % Sub-sub-section
You can list all the available parameters by typing
\begin{mdframed}[backgroundcolor=gray!20]
python3 sonicparanoid.py \--\--help
\end{mdframed}
Following is a list of SonicParanoid's parameters and their use:

\begin{mdframed}[backgroundcolor=white!20]
\textbf{-i INPUT\_DIRECTORY, \--\--input-directory INPUT\_DIRECTORY}\\
Directory containing the proteomes (in FASTA format) of the species to be compared. NOTE: the file names MUST NOT contain the '-' nor '.' characters\\\\
\textbf{-o OUTPUT\_DIRECTORY, \--\--output-directory OUTPUT\_DIRECTORY}\newline
The directory in which the results will be stored.\\\\
\textbf{-t THREADS, \--\--threads THREADS}\\
Number of parallel 1-CPU threads to be used. Default=4.\\\\
\textbf{-u UPDATE\_NAME, \--\--update UPDATE\_NAME}\\
Update the ortholog tables database by adding or removing input proteomes. Performs only required alignments (if any) for new species pairs if required and re-compute the ortholog groups. NOTE: an ID (UPDATE\_NAME) for the update must be provided.\\\\
\textbf{-m \{ultra-fast, fast, default\}, \--\--mode \{ultra-fast, fast, default\} }\\
SonicParanoid execution mode. The fast mode is suggested for most type of studies. Use default when comparing evolutionary distant species.\\Default = default\\\\
\textbf{-se, \--\--sensitivity}\\
Sensitivity for MMseqs2 [1, 7.5]. This will overwrite the \--\--mode parameter\\\\
\textbf{-ml, \--\--max-len-diff}\\
Maximum allowed length difference between main orthologs and canditate inparalogs.\\
Example: 0.5 means one of the sequences could be 2 times longer than the other;\\
0 means no length difference allowed;\\
1 means no restriction is applied on lenght difference.\\
Default = 0.5\\\\
\textbf{-ot, \--\--overwrite-tables}\\
This will force the re-computation of the ortholog tables. Only missing alignment files will be re-computed.\\\\
\textbf{-ow, \--\--overwrite}\\
Overwrite previous runs and execute it again. This can be useful to update a subset of the computaed tables.\\\\
\textbf{-sm, \--\--skip-multi-species }\\
Skip the creation of multi-species ortholog groups.
\end{mdframed}

%----------------------------------------------------------------------------------------
%	BENCHMARKING
%----------------------------------------------------------------------------------------

\section{Becnhmarks} % Sub-section
SonicParanoid was benchmarked using its three execution modes (\textit{ultra-fast}, \textit{fast}, and \textit{default}), using the Orthology Benchmarking service from the QfO consortium.

\section{Test data}
SonicParanoid was tested using a benchmark proteome dataset from the Quest for Orthologs consortium (QfO), composed of 66 proteomes, 40 of which from eukaryotes, 5 archaea and 21 bacteria.\\
The dataset is avaliable at the following link:\\
\href{ftp://ftp.ebi.ac.uk/pub/databases/reference_proteomes/previous_releases/qfo_release-2011_04}{ftp://ftp.ebi.ac.uk/pub/databases/reference\_proteomes/previous\_releases/qfo\_release-2011\_04}

\section{Obtaining help}
The fastest way to get scientific, and technical support is through the following Gitter chat-room:\\
\href{https://gitter.im/SonicParanoid/Lobby}{https://gitter.im/SonicParanoid/Lobby}\\
which is also avaliable at \href{http://iwasakilab.bs.s.u-tokyo.ac.jp/sonicparanoid/}{SonicPanoid's web-page}.

\sethlcolor{red}
\section{License}
Copyright \copyright\ 2017, Salvatore Cosentino, \href{http://www.u-tokyo.ac.jp}{The University of Tokyo} All rights reserved.\\
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at \href{http://www.apache.org/licenses/LICENSE-2.0}{http://www.apache.org/licenses/LICENSE-2.0}\\
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an \hl{"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND}, either express or implied. See the License for the specific language governing permissions and limitations under the License.

\section{Contact}
Salvatore Cosentino\\
\href{mailto:salvocos@bs.s.u-tokyo.ac.jp}{salvocos@bs.s.u-tokyo.ac.jp}\\
\href{mailto:salvo981@gmail.com}{salvo981@gmail.com}\\\\
Wataru Iwasaki\\
\href{mailto:iwasaki@bs.s.u-tokyo.ac.jp}{iwasaki@bs.s.u-tokyo.ac.jp}\\

%----------------------------------------------------------------------------------------
%	BIBLIOGRAPHY
%----------------------------------------------------------------------------------------

%\begin{thebibliography}{99} % Bibliography - this is intentionally simple in this template
%
%\bibitem[Figueredo and Wolf, 2009]{Figueredo:2009dg}
%Figueredo, A.~J. and Wolf, P. S.~A. (2009).
%\newblock Assortative pairing and life history strategy - a cross-cultural
%  study.
%\newblock {\em Human Nature}, 20:317--330.
%
%\end{thebibliography}

%----------------------------------------------------------------------------------------

\end{document}
